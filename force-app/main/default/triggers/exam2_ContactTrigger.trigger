/**
 * @description       :
 * @author            : Viktor Bogdan
 * @group             :
 * @last modified on  : 08-10-2021
 * @last modified by  : Viktor Bogdan
 **/
trigger exam2_ContactTrigger on Contact(before insert, before update, after insert, after update) {
  if (Trigger.isBefore && Trigger.isInsert) {
    ContactTriggerHandlerSec6.addAccountToContact(Trigger.new);
  }
}
