import { LightningElement, api, track, wire } from 'lwc';
import ID_FIELD from '@salesforce/schema/Contact.Id';
import NAME_FIELD from '@salesforce/schema/Contact.Name';
import PHONE_FIELD from '@salesforce/schema/Contact.Phone';
import ACCOUNT_ID_FIELD from '@salesforce/schema/Contact.AccountId';
import DEPARTMENT_FIELD from '@salesforce/schema/Contact.Department';
import Email_FIELD from '@salesforce/schema/Contact.Email';
import CONFIDENTIAL_FIELD from '@salesforce/schema/Contact.Confidential__c';
import CREATED_DATE_FIELD from '@salesforce/schema/Contact.CreatedDate';
import getContacts from '@salesforce/apex/exam2_ContactContoller.getContacts';
import createContact from '@salesforce/apex/exam2_ContactContoller.createContact';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const columns = [
    { label: 'Id', fieldName: ID_FIELD.fieldApiName },
    { label: 'Name', fieldName: NAME_FIELD.fieldApiName },
    { label: 'Phone', fieldName: PHONE_FIELD.fieldApiName },
    { label: 'Account Id', fieldName: ACCOUNT_ID_FIELD.fieldApiName },
    { label: 'Department', fieldName: DEPARTMENT_FIELD.fieldApiName },
    { label: 'Email', fieldName: Email_FIELD.fieldApiName, type: 'email' },
    { label: 'Confidential', fieldName: CONFIDENTIAL_FIELD.fieldApiName },
    { label: 'CreatedDate', fieldName: CREATED_DATE_FIELD.fieldApiName, type: 'date' }
];

export default class Exam2_ContactTable extends LightningElement {
    @track contacts;
    @track contact;
    @track error;
    @track accessError;
    contactId;
    columns = columns;
    nameField = NAME_FIELD;
    phoneField = PHONE_FIELD;

    @api recordId;

    connectedCallback() {
        console.log('connectedCallback action');
        this.loadContacts();
    }

    handleClick() {
        console.log('handleClick');
        const firstName = this.template.querySelector("lightning-input.firstName");
        const lastName = this.template.querySelector("lightning-input.lastName");
        const department = this.template.querySelector("lightning-input.department");
        const phone = this.template.querySelector("lightning-input.phone");
        this.contact = {
            FirstName: firstName.value,
            LastName: lastName.value,
            Department: department.value,
            Phone: phone.value
        };
        this.createCon();

    }


    createCon() {
        createContact({ cont: JSON.stringify(this.contact) })
            .then(result => {
                console.log('---searchCons---then')
                this.contactId = result;
                this.error = null;
                this.handleSuccess();
                this.loadContacts();
                console.log('---searchCons---then  2');

            })
            .catch(error => {
                console.log('---searchCons---catch')
                this.error = error;
                this.contacts = null;
            })
    }

    loadContacts() {
        console.log('---loadContacts')

        getContacts()
            .then(result => {
                console.log('---loadContacts   then')
                this.contacts = result;

                console.log('---loadContacts   then2 ' + result);

            })
            .catch(error => {
                this.error = error;
                console.dir('error  -------');
                console.log(JSON.stringify(error.body.message));
                console.dir(error.body.message[0].message);
            })
    }


    handleSuccess() {
        console.log('---handleSuccess');

        const evt = new ShowToastEvent({
            title: "Contact created",
            message: "Record ID: " + this.contactId,
            variant: "success"
        });
        this.dispatchEvent(evt);
    }


    // @wire(getContacts)
    // test1({ error, data }) {
    //     if (data) {
    //         console.log(' @wire(searchContactByName');
    //         this.contacts = data;
    //         this.error = this.contacts.length === 0 ? true : false;
    //     } else if (error) {
    //         this.error = error;
    //         this.contacts = undefined;
    //     }
    // }

}
