/**
 * @description       : 
 * @author            : Viktor Bogdan
 * @group             : 
 * @last modified on  : 08-10-2021
 * @last modified by  : Viktor Bogdan
**/
@isTest
public with sharing class exam2_ContactContollerTest {

@isTest
   public static void getContactsTest() {
    exam2_TestDataFactory.createContacts(15);
    List<Contact> res = exam2_ContactContoller.getContacts();
    System.assertEquals(res.size(),15);
   }


   @isTest
   public static void createContact() {
    exam2_TestDataFactory.createContacts(10);
    exam2_TestDataFactory.createAccounts(5);
    Contact con = new Contact(FirstName='joe', LastName='dfasdf');

    exam2_ContactContoller.createContact(JSON.serialize(con));
    List<Contact> res = [SELECT id FROM Contact];
    List<Contact> res1 = [SELECT id FROM Contact where LastName='dfasdf'];

    System.assertEquals(res.size(),11);
   }

}
