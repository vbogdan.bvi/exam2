/**
 * @description       :
 * @author            : Viktor Bogdan
 * @group             :
 * @last modified on  : 08-10-2021
 * @last modified by  : Viktor Bogdan
 **/
public with sharing class exam2_ContactTriggerHandler {
  public static void addAccountToContact(List<Contact> newContacts) {
    List<Account> accounts = [SELECT Name, Id FROM Account  ORDER BY Name];
    if (accounts.isEmpty()) {
      return;
    }

    List<AggregateResult> results = [
      SELECT AccountId, count(Id) kol
      FROM Contact
      GROUP BY AccountId
    ];

    List<AccountWrapper> accountsWrList = new List<AccountWrapper>();

    for (Integer i = 0; i < accounts.size(); i++) {
      AccountWrapper accWr = new AccountWrapper();
      accWr.acc = accounts[i];
      for (Integer j = 0; j < results.size(); j++) {
        if (accWr.acc.Id == results[j].get('AccountId')) {
          accWr.kolContacts = Integer.valueOf(results[j].get('kol'));
          continue;
        }
      }
      system.debug(accWr);
      accountsWrList.add(accWr);
    }

    sortAccounts(accountsWrList);

    Integer j = 0;
    for (Contact cont : newContacts) {
      cont.AccountId = accountsWrList[0].acc.Id;
      sortAccounts(accountsWrList);
    }
  }

  /**
   * @param accounts {List<AccountWrapper>}
   **/
  public static void sortAccounts(List<AccountWrapper> accWrs) {
    System.debug('sortAccounts');
    for (Integer i = 0; i < accWrs.size(); i++) {
      AccountWrapper accWr = accWrs[i];
      Integer index = i;
      for (Integer j = i + 1; j < accWrs.size(); j++) {
        if (accWrs[j].kolContacts < accWr.kolContacts) {
          accWr = accWrs[j];
          index = j;
        }
      }
      if (i != index) {
        AccountWrapper tmp = accWrs[i];
        accWrs[i] = accWrs[index];
        accWrs[index] = tmp;
      }
    }
  }

  private class AccountWrapper {
    public Account acc;
    public Integer kolContacts = 0;
  }
}
