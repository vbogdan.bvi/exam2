/**
 * @author            : Viktor Bogdan
 * @last modified on  : 08-10-2021
 * @last modified by  : Viktor Bogdan
 **/
public with sharing class exam2_ContactContoller {
  @AuraEnabled
  public static List<Contact> getContacts() {
    system.debug('getContacts');

    try {
      List<Contact> contacts = [
        SELECT
          Id,
          Name,
          Phone,
          Account.Name,
          AccountId,
          Department,
          Email,
          Confidential__c,
          CreatedDate
        FROM Contact
        ORDER BY CreatedDate DESC
      ];
      return contacts;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  @AuraEnabled
  public static Id createContact(String cont) {
    try {
      system.debug('createContact   ' + Cont);
      List<Contact> contacts = new List<Contact>();
      Contact con = (Contact) JSON.deserialize(cont, Contact.class);
      contacts.add(con);
      upsert contacts;
      return con.Id;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

  /*
  @AuraEnabled
  public static void updateRecordInDbApexContr(String rec) {
      System.debug('updateRecordInDbApexContr   ' + rec);
    try {
        // throw new AuraHandledException('test');
        List<Contact> contacts = new List<Contact>();
        Contact con = (Contact) JSON.deserialize(rec, Contact.class);
        System.debug(con);
        contacts.add(con);
        if(con.LastName == 'Error contact'){
            throw new AuraHandledException('test');
        }
      update contacts;
    } catch (Exception e) {
      throw new AuraHandledException(e.getMessage());
    }
  }

    */
}