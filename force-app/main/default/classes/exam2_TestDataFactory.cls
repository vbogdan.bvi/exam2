/**
 * @description       :
 * @author            : Viktor Bogdan
 * @group             :
 * @last modified on  : 08-10-2021
 * @last modified by  : Viktor Bogdan
 **/
@isTest
public with sharing class exam2_TestDataFactory {
  public static List<Contact> createContacts(Integer numCons) {
    List<Contact> conts = new List<Contact>();

    for (Integer i = 0; i < numCons; i++) {
      Contact a = new Contact(LastName = 'TestContact' + i);
      conts.add(a);
    }
    insert conts;
    return conts;
  }

  public static List<Account> createAccounts(Integer num) {
    List<Account> accs = new List<Account>();

    for (Integer i = 0; i < num; i++) {
      Account a = new Account(Name = 'TestAcc' + i);
      accs.add(a);
    }
    insert accs;
    return accs;
  }
}
